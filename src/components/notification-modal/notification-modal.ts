import { Component } from '@angular/core';
import {NavParams, ViewController} from "ionic-angular";

/**
 * Generated class for the NotificationModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'notification-modal',
  templateUrl: 'notification-modal.html'
})
export class NotificationModalComponent {

  message = 'notification message';

  constructor(params: NavParams,
              public viewCtrl: ViewController) {
    this.message = params.get('notificationMessage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
