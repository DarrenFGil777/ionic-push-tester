import { Component } from '@angular/core';
import {Platform, ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';

import { Firebase } from '@ionic-native/firebase';
import { Events } from 'ionic-angular';




@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
              firebase: Firebase,
              toastCtrl: ToastController,
              public events: Events
              ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      firebase.getToken().then(token => console.log(token)).catch(err=> console.log(err));
      firebase.onNotificationOpen().subscribe(data=>{
        console.log('data');
        console.log(data);
        events.publish('notification:event', data.body);
        console.log('data body');
        console.log(data.body);
      }, err=> console.log(err));

    });
  }
}
