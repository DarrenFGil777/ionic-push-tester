import { Component } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';

import { Events } from 'ionic-angular';
import { ModalController, NavParams} from 'ionic-angular';
import {NotificationModalComponent} from "../../components/notification-modal/notification-modal";



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
              public events: Events,
              public modalCtrl: ModalController) {
    events.subscribe('notification:event', (notification) => {
      console.log('notification');
      console.log(notification);
      let profileModal = this.modalCtrl.create(NotificationModalComponent, { notificationMessage: notification });
      profileModal.present();
    });
  }

  modal() {
    let profileModal = this.modalCtrl.create(NotificationModalComponent, { notificationMessage: 8675309 });
    profileModal.present();
  }

}
